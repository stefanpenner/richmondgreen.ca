function googlemap(){
  if(typeof GBrowserIsCompatible !== 'undefined' ){
    var map = new GMap2(document.getElementById("map")),
        mapCenter = new GLatLng(49.796032,-97.167871),
        pointSW = new GLatLng(49.794369,-97.170344),
        pointNE = new GLatLng(49.796697,-97.166857),
        siteMap = new GGroundOverlay( "images/overlay.png", new GLatLngBounds(pointSW, pointNE));

    map.setMapType(G_HYBRID_MAP);
    map.setCenter(mapCenter, 17);
    map.addOverlay(siteMap); 
    map.addControl(new GSmallMapControl());
    map.addControl(new GMapTypeControl());
  }
}

// showcase viewer
var Chromium = Chromium || {};

(function(lib) {
  lib.konami = function(){
    if ( window.addEventListener ) {
      var state = 0, konami = [38,38,40,40,37,39,37,39,66,65];
      window.addEventListener("keydown", function(e) {
        if ( e.keyCode == konami[state] ) state++;
        else state = 0;
        if ( state == 10 )
          window.location = "http://53cr.com/secret";
      }, true);
    }
  }
})(Chromium);

(function(lib){
  var url = document.URL,
      api = {};

  api.path = function(regex,fcn){
    var result = url.match(regex);
    if(result){ 
      fcn(result);
    }
  };

  lib.router = function(fcn){
    fcn(api);
  };
})(Chromium);

$(document).ready(function(){
  $("img").hide().load().fadeIn('slow');
  Chromium.konami();
  Chromium.router(function(map){
    map.path(/(floorplans|gallery).html/, function(){
      $("#showcase-maple ul li a, #showcase ul li a").act_as_showcase();
    });

    map.path(/location.html/, function(){
      googlemap();
    });

    map.path(/gallery/,function(){
      $('#tabs-menu').tabs("#tab", { effect : 'ajax' });
    });
  });
});

// showcase
(function($){  
 	$.fn.act_as_showcase = function(){  
   		var	showcase = function(event){
			event.preventDefault();
			var img = new Image();
			$('#viewer img').remove();
			var viewer = $('#viewer');
			viewer.addClass('loading');
			
			$(img).load(function(){
				viewer.hide();
				viewer.removeClass('loading').append(this);
				viewer.fadeIn('slow');
			}).attr('src',this.href);
		};
	return $(this).click(showcase);  
	};
})(jQuery);

